#include <algorithm>
#include <iostream>
#include <fstream>
#include <random>
#include <numeric>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <cmath>
#include <ctime>
#include <cstdint>
using namespace std;
using namespace std::chrono;

int aleatorio(int min, int max)
{
    thread_local auto threadID = std::hash<thread::id>()(this_thread::get_id());
    thread_local default_random_engine gen(static_cast<uint32_t>(time(nullptr)) + threadID);
    uniform_int_distribution<> dist(min, max);
    return dist(gen);
}

struct Tarefa
{
    int id;
    int tempo;

    Tarefa(int id, int tempo) : id(id), tempo(tempo) {}
    Tarefa() : Tarefa(0, 0) {}
};

struct Maquina
{
    int id;
    vector<Tarefa> tarefas;

    Maquina(int id, const vector<Tarefa>& tarefas) : id(id), tarefas(tarefas) {}
    Maquina() : id(0), tarefas() {}

    int soma() const
    {
        int n = 0;
        for(const Tarefa& tarefa : tarefas) {
            n += tarefa.tempo;
        }
        return n;
    }

    vector<Tarefa> listaDecrescente() const
    {
        vector<Tarefa> saida = tarefas;
        sort(saida.begin(), saida.end(), [](Tarefa a, Tarefa b) {
            return a.tempo > b.tempo;
        });
        return saida;
    }
};

struct ResultadoMakespan
{
    const size_t indiceMaquina;
    const Maquina* maquina;
    const int valor;

    ResultadoMakespan(size_t i, const Maquina* m, int v) : indiceMaquina(i), maquina(m), valor(v) {}
};

vector<Tarefa> geraTarefas(int maquinas, double fator)
{
    vector<Tarefa> saida;
    double tamanho = lround(pow(maquinas, fator));
    saida.resize(static_cast<size_t>(tamanho));
    for(size_t i = 0; i < saida.size(); i++) {
        saida[i].id = static_cast<int>(i);
        saida[i].tempo = aleatorio(1, 100);
    }
    return saida;
}

struct Algoritmo
{
    vector<Maquina> maquinas;
    int numIteracoes;

    Algoritmo(int numMaquinas, const vector<Tarefa>& tarefas)
        : maquinas(static_cast<size_t>(numMaquinas))
        , numIteracoes(0)
    {
        for(size_t i = 0; i < maquinas.size(); i++) {
            maquinas[i].id = static_cast<int>(i);
        }
        maquinas[0].tarefas = tarefas;
    }

    vector<Maquina> listaDecrescente() const
    {
        vector<Maquina> saida = maquinas;
        sort(saida.begin(), saida.end(), [](Maquina a, Maquina b) {
            return a.soma() > b.soma();
        });
        return saida;
    }

    static ResultadoMakespan calcMakespan(const vector<Maquina>& maquinas)
    {
        int makespan = 0;
        size_t indice = 0;
        for(size_t i = 0; i < maquinas.size(); i++) {
            int soma = maquinas[i].soma();
            if(soma > makespan) {
                makespan = soma;
                indice = i;
            }
        }
        return ResultadoMakespan(indice, &maquinas[indice], makespan);
    }

    ResultadoMakespan calcMakespan() const
    {
        return calcMakespan(this->maquinas);
    }

    static int makespan(const vector<Maquina>& maquinas)
    {
        return calcMakespan(maquinas).valor;
    }

    int makespan() const
    {
        return makespan(this->maquinas);
    }
};

struct BuscaLocalMonotona : public Algoritmo
{
    BuscaLocalMonotona(int numMaquinas, const vector<Tarefa>& tarefas, double)
        : Algoritmo(numMaquinas, tarefas)
    {}

    const char* getNome() const
    {
        return "monotona";
    }


    bool avancaIteracao()
    {
        numIteracoes++;
        auto candidatos = listaDecrescente();
        // Máquina mais ocupada
        auto& maquinaA = candidatos.front();
        // Máquina menos ocupada
        auto& maquinaB = candidatos.back();
        maquinaB.tarefas.push_back(maquinaA.tarefas.back());
        maquinaA.tarefas.pop_back();
        // Desiste se não houve melhora
        if(makespan() <= makespan(candidatos)) {
            return false;
        }
        // Se não, grava a nova lista e continua
        else {
            maquinas = candidatos;
            return true;
        }
    }
};

struct BuscaHeuristicaTabu : public Algoritmo
{
    static const int criterioParada = 1000;

    struct Movimento
    {
        int idade;
        int idTarefa;

        Movimento(int tarefa) : idade(0), idTarefa(tarefa) {}
    };

    vector<Movimento> listaTabu;
    vector<Maquina> melhorSolucao;
    const int idadeMaxima;
    const double alfa;
    int melhorMakespan;
    int tentativas;

    BuscaHeuristicaTabu(int numMaquinas, const vector<Tarefa>& tarefas, double alfa)
        : Algoritmo(numMaquinas, tarefas)
        , idadeMaxima(lround(alfa * tarefas.size()))
        , alfa(alfa)
        , melhorMakespan(makespan())
        , tentativas(0)
    {}

    const char* getNome() const
    {
        return "buscatabu";
    }

    bool avancaIteracao()
    {
        numIteracoes++;
        // Processa o envelhecimento da lista
        for(Movimento& mov : listaTabu) {
            mov.idade++;
        }
        remove_if(listaTabu.begin(), listaTabu.end(), [this](Movimento movimento){
            return movimento.idade > idadeMaxima;
        });
        // Tenta realizar o movimento da primeira tarefa fora da lista tabu
        auto candidatos = listaDecrescente();
        auto& maquinaB = candidatos.back();
        for(Maquina& maquinaA : candidatos) {
            // Ordena as tarefas de maior para menor, dando mais consistência à heurística
            maquinaA.tarefas = maquinaA.listaDecrescente();
            for(size_t tid = 0; tid < maquinaA.tarefas.size(); tid++) {
                const auto& tarefaAlvo = maquinaA.tarefas[tid];
                // Verirfica se a tarefa está na lista tabu
                bool proibida = false;
                for(Movimento movimento : listaTabu) {
                    if(tarefaAlvo.id == movimento.idTarefa) {
                        proibida = true;
                        break;
                    }
                }
                if(!proibida) {
                    // Adiciona essa tarefa na lista tabu
                    listaTabu.emplace_back(tarefaAlvo.id);
                    // Move a tarefa
                    maquinaB.tarefas.push_back(tarefaAlvo);
                    // Remoção rápida: faz a troca com o último elemento e remove-o
                    maquinaA.tarefas[tid] = maquinaA.tarefas.back();
                    maquinaA.tarefas.pop_back();
                    goto fim_for_for;
                }
            }
        }
        fim_for_for:
        maquinas = candidatos;
        int novoMakespan = makespan();
        // Se houve melhora global, grava a nova solução
        if(melhorMakespan > novoMakespan) {
            melhorSolucao = candidatos;
            melhorMakespan = novoMakespan;
            tentativas = 0;
            return true;
        }
        // Se não houve melhora ou a melhora foi apenas local, continua
        else {
            tentativas++;
            // Retorna à melhor solução e pára após o limite
            if(tentativas >= criterioParada) {
                maquinas = melhorSolucao;
                return false;
            }
            return true;
        }
    }
};

template<typename TAlgoritmo>
void soluciona(int maquinas, double fator, double alfa, size_t replicacoes, ostream& saida)
{
    vector<thread> threads(replicacoes);
    mutex mutexSaida;
    for(size_t i = 0; i < replicacoes; i++) {
        threads[i] = thread([=, &saida, &mutexSaida] {
            vector<Tarefa> instancia = geraTarefas(maquinas, fator);
            TAlgoritmo algo(maquinas, instancia, alfa);

            auto inicio = high_resolution_clock::now();
            while(algo.avancaIteracao());
            auto final = high_resolution_clock::now();
            auto tempo = duration_cast<milliseconds>(final - inicio).count();

            // Imprime no .CSV
            mutexSaida.lock();
            saida << algo.getNome() << ", "
                  << instancia.size() << ", "
                  << maquinas << ", "
                  << i+1 << ", "
                  << tempo << ", "
                  << algo.numIteracoes << ", "
                  << algo.makespan() << ", ";
            if(alfa > 0) {
                saida << alfa << "\n";
            }
            else {
                saida << "NA\n";
            }
            mutexSaida.unlock();
        });
    }
    // Espera os algoritmos terminarem
    for(thread& th : threads) {
        th.join();
    }
}

int main()
{
    ofstream arquivo;
    arquivo.open("resultados.csv", ios_base::out | ios_base::trunc);
    if(!arquivo.is_open()) {
        cerr << "Não foi possível criar arquivo para escrita dos resultados" << endl;
        return 1;
    }

    // Cabeçalho
    arquivo << "heuristica, n, m, replicacao, tempo, iteracoes, valor, parametro\n";

    cout << "Executando busca local monótona..." << endl;
    for(int maquinas : {10, 20, 50}) {
        for(double fator : {1.5, 2.0}) {
            cout << "  m: " << maquinas << ", f: " << fator << endl;
            soluciona<BuscaLocalMonotona>(maquinas, fator, 0, 10, arquivo);
            arquivo.flush();
        }
    }

    cout << "Executando busca tabu heurística..." << endl;
    for(int maquinas : {10, 20, 50}) {
        for(double fator : {1.5, 2.0}) {
            cout << "  m: " << maquinas << ", f: " << fator << endl;
            for(double alfa : {0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09}) {
                cout << "    a: " << alfa << endl;
                soluciona<BuscaHeuristicaTabu>(maquinas, fator, alfa, 10, arquivo);
                arquivo.flush();
            }
        }
    }

    arquivo.close();
    return 0;
}
